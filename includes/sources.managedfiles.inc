<?php

/**
 * @file
 * Contains the backup_migrate_source_managedfiles class.
 */

/**
 * Backup and Migrate source for remote file storage.
 */
class backup_migrate_source_managedfiles extends backup_migrate_source {

  public $supported_ops = array('restore', 'configure', 'delete', 'source');

  /**
   * Returns the name of this source type.
   *
   * @return string
   *   The translated name of this source type.
   */
  public function type_name() {
    return t("Managed Files");
  }

  /**
   * Return the pre-defined sources for this sources type.
   *
   * @return array
   *   An array of BAM source,one for each visible stream wrapper.
   */
  public function sources() {
    $out = array();
    // Define one source for each visible stream wrapper.
    $wrappers = file_get_stream_wrappers(STREAM_WRAPPERS_VISIBLE);
    foreach ($wrappers as $scheme => $wrapper) {
      $out["{$scheme}_files"] = backup_migrate_create_source('managedfiles', array(
        'machine_name' => "file_managed_{$scheme}",
        'location' => "$scheme://",
        'name' => t('Managed @name Files', array(
          '@name' => trim(str_replace('files', '', $wrapper['name'])),
        )),
        'show_in_list' => TRUE
      ));
    }
    return $out;
  }

  /**
   * {@inheritdoc}
   */
  public function edit_form() {
    $form = parent::edit_form();
    $form['location'] = array(
      "#type" => "textfield",
      "#title" => t("Directory path"),
      "#default_value" => $this->get_location(),
      "#required" => TRUE,
      "#description" => t('The path of the directory to backup, including its scheme.'),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function file_types() {
    return array(
      "tar" => array(
        "extension" => "tar",
        "filemime" => "application/x-tar",
        "backup" => TRUE,
        "restore" => TRUE,
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function backup_settings_default() {
    return array(
      'exclude_filepaths' => "backup_migrate\nstyles\ncss\njs\nctools\nless\nlanguages",
    );
  }

  /**
   * {@inheritdoc}
   */
  public function backup_settings_form($settings) {
    $form['exclude_filepaths'] = array(
      "#type" => "textarea",
      "#multiple" => TRUE,
      "#title" => t("Exclude the following files or directories"),
      "#default_value" => $settings['exclude_filepaths'],
      "#description" => t("A list of files or directories to be excluded from backups. Add one path per line relative to the directory being backed up."),
    );
    return $form;
  }

  /**
   * Backup this source.
   *
   * @param backup_file $file
   *   The backup file to backup files to.
   * @param array $settings
   *   The backup settings.
   *
   * @return backup_file|bool
   *   The passed in backup file, or FALSE on failure.
   */
  public function backup_to_file(backup_file $file, $settings) {

    // Query the file_managed table for the files to backup.
    $query = db_select('file_managed', 'fm')
      ->fields('fm')
      // Must be inside the source's location.
      ->condition('uri', $this->get_location() . '%', 'LIKE')
      // Must be permanent.
      ->condition('status', FILE_STATUS_PERMANENT);
    foreach ($this->get_excluded_paths($settings) as $exclude) {
      // Must not be excluded.
      $query->condition('uri', $exclude , 'NOT LIKE');
      // Must not be inside and excluded path.
      $query->condition('uri', $exclude . '/&', 'NOT LIKE');
    }
    $files = $query->execute()
      ->fetchAll();

    /** @var array $errors The list of files that could not be read and were skipped. */
    $errors = array();

    // Fail if there is not file to backup from source.
    if (!$files) {
      backup_migrate_backup_fail('No @name to backup.', array('@name' => strtolower($settings->source->name)), $settings);
      return FALSE;
    }
    // Backup to file to a .tar archive, one file at a time.
    // This allow proper detection of each failed file.
    else {
      $file->push_type('tar');
      $archive = new Archive_Tar($file->filepath(), FALSE);

      watchdog('backup_migrate', "Found @count @name (@size) to backup.", array(
        '@name' => strtolower($settings->source->name),
        '@count' => count($files),
        '@size' => format_size(array_reduce($files, function($size, $file) {
          return $size + $file->filesize;
        })),
      ), WATCHDOG_NOTICE);

      /** @var object $file */
      foreach ($files as $row) {
        try {
          watchdog('backup_migrate', "Adding @uri (@size) to @archive ...", array(
            '@uri' => $row->uri,
            '@size' => format_size($row->filesize),
            '@archive' => $file->filepath(),
          ), WATCHDOG_DEBUG);
          // This will throw an exception if the file does not exists or is not readable.
          $archive->addModify($row->uri, '', $this->get_location());
        }
        catch (Exception $e) {
          watchdog_exception('backup_migrate', $e, "Cannot backup @uri: !message.", array('@uri' => $row->uri), WATCHDOG_DEBUG);
          // Fail the backup if erros are not ignored.
          if (empty($settings->filters['ignore_errors'])) {
            backup_migrate_backup_fail('The backup could not be completed because !files could not be read. If you want to skip unreadable files use the \'Ignore Errors\' setting under \'Advanced Options\' in \'Advanced Backup\' or in your schedule settings profile.', array('!files' => $file->uri), 'error');
            return FALSE;
          }
          // Continue, but track the error.
          else {
            $errors[] = $row->uri;
          }
        }
      }

      if ($errors) {
        if (count($errors < 5)) {
          $filesmsg = t('The following files: !files', array('!files' => theme('item_list', array('items' => $errors))));
        }
        else {
          $filesmsg = t('!count files', array('!count' => count($errors)));
        }
        _backup_migrate_message('!files could not be read and were skipped', array('!files' => $filesmsg), 'error');
      }
      return $file;
    }
  }

  /**
   * Restore to this source.
   *
   * @param backup_file $file
   *   The backup file to restore files from.
   * @param array $settings
   *   The backup settings.
   *
   * @return backup_file|bool
   *   The passed in backup file, or FALSE on failure.
   */
  public function restore_from_file(backup_file $file, &$settings) {
    $from = $file->pop_type();

    $tar = new Archive_Tar($from->filepath());

    // Extract archive content, one file at a time. This allow restoration of
    // as many file as possible (vs. failure on the first error).
    foreach ($tar->listContent() as $item) {
      try {
        $tar->extractList($item['filename'], $this->get_location());
      }
      catch (Exception $e) {
        watchdog_exception('Backup and Migrate', $e, "Unable to restore @filename to @location: !message.", array(
          '@filename' => $item['filename'],
          '@location' => $this->get_location(),
        ), WATCHDOG_ERROR);
      }
    }

    return $file;
  }

  /**
   * Break the excluded paths string into a usable list of paths.
   *
   * @param array $settings
   *   The backup settings.
   *
   * @return array
   *   An array of paths (strings) excluded from backups.
   */
  protected function get_excluded_paths($settings) {
    $base_dir = $this->get_location() . '/';
    $base_scheme = file_uri_scheme($base_dir);
    $paths = empty($settings->filters['exclude_filepaths']) ? '' : $settings->filters['exclude_filepaths'];
    $excludes = explode("\n", $paths);

    foreach ($excludes as $key => $exclude) {
      $scheme = file_uri_scheme($exclude);
      // If the path as no scheme, preprend the location.
      if (!$scheme) {
        $excludes[$key] = $base_dir . $exclude;
      }
      // If the path ison a different scheme, ignore it.
      elseif ($scheme != $base_scheme) {
        unset($excludes[$key]);
      }
      // If the path is not part of the location, ignore it.
      elseif (substr($exclude, 0, strlen($base_dir)) != $base_dir) {
        unset($excludes[$key]);
      }
    }

    return $excludes;
  }

}
